angular.module('myApp',['ngAnimate'])
.controller('MyController',['$scope', '$timeout', '$document',function($scope, $timeout, $document){
    $scope.rotat = {
      'float':'left',
      '-webkit-transition':'all 1s ease',
      'transition':'all 1s ease',
      '-webkit-transform':'rotateY(1080deg)',
      'transform':'rotateY(1080deg)'
    };

    $scope.canvasstyle = {
      'background-color': '#6495ED',
      'background-image': 'url("img/jimen.gif")',
      'background-repeat': 'repeat-x',
      'background-position': 'left bottom'
    };

    $scope.backg = {
      'background-color': '#000000',
      'background-image': 'url("img/3-3.png")',
      'background-repeat': 'repeat-x',
      'background-position': 'left bottom'
    };

    $scope.checkValue = false;
    var defaultarr = new Array();
    var countarr = new Array();
    for(a=0; a<75; a++){
      defaultarr[a]={
        count : a+1,
        flg : false
      };
      countarr[a] = a+1;
    }

    var arr = new Array();
    var count = 0;
    for(i=0; i<5; i++){
      arr[i] = new Array();
      for(j=0; j<15; j++){
        arr[i][j] = {
          img : 'img/' + defaultarr[count]['count'] + '_.png',
          flg : defaultarr[count]['flg']
        };
        count++;
      }
    }
    $scope.items = arr;

    var canvas = $document.find('canvas')[0];
    canvas.width = 960;
    canvas.height = 180;

    $scope.onclick = function(){

      $scope.checkValue = true;
      if(countarr.length==0){
        return;
      }

      var rand = Math.floor(Math.random()*(countarr.length));
      var x = 0;
      var y = 0;
      var x_zahyo = 0;
      var ctx = canvas.getContext('2d');
      mro = new Image();
      mro.src = "img/mario.gif";
      nokonoko = new Image();
      nokonoko.src = "img/nokonoko.gif";
      function draw(){
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        var position = x_zahyo - 50;
        if(rand % 2 === 0){
          ctx.drawImage(mro, position, canvas.height-80, 50, 50);
        } else {
          ctx.drawImage(nokonoko, position, canvas.height-80, 50, 50);
        }
        x_zahyo += 10;
        requestAnimationFrame(draw);
          cancelAnimationFrame(draw);
      }
      draw();
      ctx.restore();

      var e = countarr[rand];


      $timeout(function(){
        $scope.isFullscreenOverlayActive = true;
        $scope.resultitem = 'img/' + defaultarr[e-1]['count'] + '.png';
        countarr.splice(rand, 1);
        defaultarr[e-1]={
          count:e,
          flg:true
        };

        arr = new Array();
        count = 0;
        for(i=0; i<5; i++){
          arr[i] = new Array();
          for(j=0; j<15; j++){
            var tmpflg = defaultarr[count]['flg'];
            var tmpimg = 'img/' + defaultarr[count]['count'] + '.png';
            if(tmpflg==false){
              tmpimg = 'img/' + defaultarr[count]['count'] + '_.png';
            }
            arr[i][j]={
              img : tmpimg,
              flg : tmpflg
            };
            count++;
          }
        }
        $timeout(function(){$scope.items = arr;},500);
      },2000);
    };

    $scope.isFullscreenOverlayActive = false;
    $scope.openFullscreenOverlay = function(){
      $scope.isFullscreenOverlayActive = true;
    }
    $scope.closeFullscreenOverlay = function(){
      $scope.isFullscreenOverlayActive = false;
      $scope.checkValue = false;
    }
}]);
